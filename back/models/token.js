
'use strict';

module.exports = (sequelize, DataTypes) => {
  const Token = sequelize.define('Token', {
    token: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    idusuari: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    nomusuari: {
      type: DataTypes.STRING,
      allowNull: false,
    }
    
  }, { tableName: 'tokens'});
  
  return Token;
};
