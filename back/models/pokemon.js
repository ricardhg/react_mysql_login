
'use strict';

module.exports = (sequelize, DataTypes) => {
  const Pokemon = sequelize.define('Pokemon', {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    nombre: DataTypes.STRING,
    caracter: DataTypes.STRING,
    
  }, { tableName: 'pokemons'});
  
  return Pokemon;
};
