const express = require('express');
const router = express.Router();
const model = require('../models/index');

router.all('/',(req,res,next)=>{
    res.setHeader("Access-Control-Allow-Origin", "*");
    res.setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.setHeader("Access-Control-Allow-Methods", "POST, GET, PUT, OPTIONS, DELETE");
    next();
});

router.all('/:xxx',(req,res,next)=>{
    res.setHeader("Access-Control-Allow-Origin", "*");
    res.setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.setHeader("Access-Control-Allow-Methods", "POST, GET, PUT, OPTIONS, DELETE");
    next();
});



/* GET llista de pokemons */
router.get('/', function (req, res, next) {

    // solo seguimos si existe token válido
    const token_recibido = req.body.token;
    if (!token_recibido) {
      return res.status(400).json({ok:false, error:"token no recibido"});
    }

    //ejemplo de PROTECCION de ruta: si TOKEN no existe, devolvemos error
    model.Token.findOne({ where: { token:token_recibido } })
    .then((token_devuelto)=> {
        if (token_devuelto){
            return  model.Pokemon.findAll();
        } else {
            throw "token incorrecto";
        }
    } )
    .then(pokemons => res.json({
        ok: true,
        data: pokemons
    }))
    .catch((error)=>res.json({ok:false, error:error}));
});

// petición de UN pokemon, con ID
router.get('/:id', function (req, res, next) {
    model.Pokemon.findOne({ where: {id: req.params.id}} )
        // .then(pokemon => pokemon.get({plain: true}))
        .then(pokemon => res.json({
            ok: true,
            data: pokemon
        }))
        .catch(error => res.json({
            ok: false,
            error: error
        }))
    });


/* POST pokemon. creamos un nuevo registro.*/
router.post('/', function(req, res, next) {
    model.Pokemon.create(req.body)
    .then((item) => item.save())
    .then((item)=>res.json({ok: true, data:item}))
    .catch((error)=>res.json({ok:false, error}))
});
 

/* PUT pokemon, actualizamos: findOne + update */
router.put('/:id', function(req, res, next) {
    model.Pokemon.findOne({ where: {id: req.params.id}} )
    .then((poke)=>
        poke.update(req.body)
    )
    .then((ret)=> res.json({
        ok: true,
        data: ret
    }))
    .catch(error => res.json({
        ok: false,
        error: error
    }));
});
 
 
/* DELETE pokemon , eliminamos con destroy */
router.delete('/:id', function(req, res, next) {
    model.Pokemon.destroy({ where: {id: req.params.id}} )
    .then((data)=>res.json({ok: true, data}))
    .catch((error)=>res.json({ok:false, error}))
});


module.exports = router;