import React, { Component } from 'react';

export default class Home extends Component {

    render() {

      console.log(this.props.cookies);
      let nom=this.props.cookies.get('nom');
      let token=this.props.cookies.get('token');

      return (
        <>
          <h1>Home</h1>
          <h5>Nombre: {(nom) ? nom : 'no nom'}
          <br />
          Token: {(token) ? token  :'no token'}
          </h5>

        </>
      );
    }
  }

