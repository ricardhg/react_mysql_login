import React, { Component } from 'react';
import {withCookies, Cookies } from 'react-cookie';
import { Redirect } from 'react-router-dom';
import {API} from './Config';

class Login extends Component {
    constructor() {
      super();
      //Set default message
      this.state = {
        nom: '',
        password: '',
        tornar: false
      }
      this.submit = this.submit.bind(this);
      this.canvia = this.canvia.bind(this);
    }

    canvia(event) {
      const v =  event.target.value;
      const n = event.target.name;
      this.setState({
          [n]: v
      });
   }

    submit(e){
     
      e.preventDefault();
      let nom = this.state.nom;
      let password = this.state.password;
      let data = { nom, password };
      
      fetch(API+'/usuaris/login', {
        method: 'POST', 
        headers: new Headers({ 'Content-Type': 'application/json' }),
        body: JSON.stringify(data)
      })
      .then(respuesta => respuesta.json())
      .then(respuesta => {
        if (respuesta.ok===false){
          throw respuesta.error;
        } else {
          return respuesta.data;
        }
      })
      .then(token => {
        console.log(token);
        if(token){
          console.log("establint cookies");
          this.props.cookies.set('nom', token.nomusuari, {path: '/'});
          this.props.cookies.set('id', token.idusuari, {path: '/'});
          this.props.cookies.set('token', token.token, {path: '/'});
          this.setState({tornar:true});
        }
      })
      .catch(err => console.log(err));
    }

  

    render() {

      if (this.state.tornar === true) {
          return <Redirect to='/' />
      }

      return (
        <>
          <h1>Login</h1>
          <form className="login-form" onSubmit={this.submit}>
            <input onChange={this.canvia} type="text" name="nom" value={this.state.nom} placeholder="nom" />
            <br />
            <input onChange={this.canvia} type="text" name="password"  value={this.state.password} placeholder="password" />
            <br />
            <button type="submit" className="button login">Login</button>
          </form>
         
        </>
      );
    }
  }

  export default withCookies(Login);