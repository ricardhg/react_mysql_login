import React, { Component } from 'react';
import {API} from './Config';

export default class Secret extends Component {
    constructor(props) {
      super(props);
      this.state = {
        message: 'Mensaje secreto no recibido...'
      }
    }

    

    componentDidMount() {
      let token =  this.props.cookies.get('token');
      console.log("token", token);
      if (token){
        fetch(API+'/usuaris/secret', {
          method: 'POST', 
          headers: new Headers({ 'Content-Type': 'application/json' }),
          body: JSON.stringify({token})
        })
        .then(resp => resp.json())
        .then(resp => {
          console.log(resp)
          if(resp.ok){
            this.setState({message: resp.msg});
          }
        })
      }
      
    }

    render() {
      return (
        <div>
          <h1>Secret</h1>
          <p>{this.state.message}</p>
        </div>
      );
    }
  }