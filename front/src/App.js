import React, { Component } from 'react';
import { BrowserRouter, Link, Route, Switch } from 'react-router-dom';
import { CookiesProvider , withCookies } from 'react-cookie';

import Home from './components/Home';
import Secret from './components/Secret';
import Login from './components/Login';
import Logout from './components/Logout';
import Register from './components/Register';

// https://medium.com/@rossbulat/using-cookies-in-react-redux-and-react-router-4-f5f6079905dc

class App extends Component {
  render() {
    return (
      <CookiesProvider>
      <BrowserRouter>
      <h1>{this.props.cookies.nom}</h1>
      <h1>{this.props.cookies.token}</h1>
        
        <ul>
          <li><Link to="/">Home</Link></li>
          <li><Link to="/login">Login</Link></li>
          <li><Link to="/logout">Logout</Link></li>
          <li><Link to="/register">Register</Link></li>
          <li><Link to="/secret">Secret</Link></li>
        </ul>
        <Switch>
          <Route exact path="/" render={() => <Home  cookies={this.props.cookies}/>} />
          <Route path="/login"  component={Login} />
          <Route path="/logout"  render={() => (<Logout cookies={this.props.cookies}/>)} />
          <Route path="/register" render={() => (<Register cookies={this.props.cookies}/>)} />
          <Route path="/secret" render={() => (<Secret cookies={this.props.cookies}/>)} />
        </Switch>
      </BrowserRouter>
      </CookiesProvider>
    );
  }
}


export default withCookies(App);